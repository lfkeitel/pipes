VERSION ?= $(shell git describe --tags --always --dirty)
BUILDTIME := $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
BUILDER := $(shell echo "`git config user.name` <`git config user.email`>")
CGO_ENABLED ?= 1

LDFLAGS := -X 'main.version=$(VERSION)' \
			-X 'main.buildTime=$(BUILDTIME)' \
			-X 'main.builder=$(BUILDER)' \
			-s -w

.PHONY: build

all: build

build:
	go build -o bin/pipes -ldflags="$(LDFLAGS)" ./cmd/pipes/...

test: go-test

go-test:
	go test ./...
