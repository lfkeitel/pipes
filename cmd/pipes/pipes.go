package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"runtime"

	"github.com/lfkeitel/pipes/src/lexer"
	"github.com/lfkeitel/pipes/src/token"
)

const (
	interactivePrompt = ">> "
)

var (
	interactive  bool
	printVersion bool

	version   = "Unknown"
	buildTime = ""
	builder   = ""
)

func init() {
	flag.BoolVar(&interactive, "i", false, "Interactive mode")
	flag.BoolVar(&printVersion, "version", false, "Print version information")
}

func main() {
	flag.Parse()

	if printVersion {
		versionInfo()
		return
	}

	sourceFile := flag.Arg(0)

	l, err := lexer.NewFile(sourceFile)
	if err != nil {
		log.Fatal(err)
	}
	for {
		t := l.NextToken()
		if t.Type == token.EOF {
			break
		}
		fmt.Printf("%s %#v\n", t.Type, t)
	}
}

func printParserErrors(out io.Writer, errors []string) {
	for _, msg := range errors {
		fmt.Fprintf(out, "ERROR: %s\n", msg)
	}
}

func versionInfo() {
	fmt.Printf(`Nitrogen - (C) 2018 Lee Keitel
Version:           %s
Built:             %s
Compiled by:       %s
Go version:        %s %s/%s
`, version, buildTime, builder, runtime.Version(), runtime.GOOS, runtime.GOARCH)
}
