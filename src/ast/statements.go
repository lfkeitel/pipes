package ast

import (
	"bytes"

	"github.com/lfkeitel/pipes/src/token"
)

type Statement interface {
	Node
	statementNode()
}

type Program struct {
	Filename   string
	Statements []Statement
}

func (p *Program) TokenLiteral() string {
	if len(p.Statements) == 0 {
		return ""
	}

	return p.Statements[0].TokenLiteral()
}

func (p *Program) String() string {
	var out bytes.Buffer

	for _, s := range p.Statements {
		out.WriteString(s.String())
	}

	return out.String()
}

type AssignStatement struct {
	Token token.Token // the token.DEF token
	Name  string
	Value *FlowStatement
}

func (a *AssignStatement) statementNode()       {}
func (a *AssignStatement) TokenLiteral() string { return a.Token.Literal }
func (a *AssignStatement) String() string {
	var out bytes.Buffer

	out.WriteString(a.Name)
	out.WriteString(" = ")
	if a.Value != nil {
		out.WriteString(a.Value.String())
	}
	out.WriteByte(';')

	return out.String()
}

type BlockStatement struct {
	Token      token.Token
	Statements []Statement
}

func (bs *BlockStatement) statementNode()       {}
func (bs *BlockStatement) TokenLiteral() string { return bs.Token.Literal }
func (bs *BlockStatement) String() string {
	var out bytes.Buffer
	l := len(bs.Statements) - 1
	for i, s := range bs.Statements {
		str := s.String()
		out.WriteString(str)
		if str[len(str)-1] != ';' {
			out.WriteByte(';')
		}

		if i < l {
			out.WriteByte(' ')
		}
	}
	return out.String()
}

type FlowStatement struct {
	Token token.Token
	Left  Pipe
	Right Pipe
}

func (fl *FlowStatement) statementNode()       {}
func (fl *FlowStatement) TokenLiteral() string { return fl.Token.Literal }
func (fl *FlowStatement) String() string {
	return "Flow statement"
}
