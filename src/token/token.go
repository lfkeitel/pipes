package token

import "strconv"

type TokenType int

// Position represents the line and column number where a token starts
// in a source file.
type Position struct {
	Line, Col int
}

// TODO: Add filename to token
type Token struct {
	Type     TokenType
	Literal  string
	Pos      Position
	Filename string
}

// All tokens in Nitrogen
const (
	Illegal TokenType = iota + 1
	EOF
	Comment
	EOL

	// Identifiers & literals
	Identifier
	Integer
	Float
	String
	UserTap
	UserPipe
	UserSink

	// Operators
	Assign
	Plus
	Dash
	Bang
	Asterisk
	Slash
	Modulo
	Dot
	Arrow

	// PlusAssign
	// MinusAssign
	// TimesAssign
	// SlashAssign
	// ModAssign

	LessThan
	GreaterThan
	LessThanEq
	GreaterThanEq
	Equal
	NotEqual

	// BitwiseAnd
	// BitwiseOr
	// BitwiseAndNot

	// ShiftLeft
	// ShiftRight

	// Delimiters
	// Comma
	Semicolon
	// Colon
	// Carrot

	// Groups and blocks
	LParen
	RParen
	LBrace
	RBrace
	LSquare
	RSquare

	// Keywords
	keywordBeg
	// LAnd
	// LOr

	// Function
	// Let
	// Const
	// True
	// False
	// If
	// Elif
	// Else
	// Return
	// Nil
	// For
	// While
	// Continue
	// Break
	// Try
	// Catch
	// Throw
	// Class
	// New
	// Pass
	// Import
	// As
	// Delete
	// Use
	keywordEnd
)

var tokens = [...]string{
	Illegal: "ILLEGAL",
	EOF:     "EOF",
	Comment: "COMMENT",
	EOL:     "EOL",

	// Identifiers & literals
	Identifier: "IDENT",
	Integer:    "NUMBER",
	Float:      "FLOAT",
	String:     "STRING",
	UserTap:    "USERTAP",
	UserPipe:   "USERPIPE",
	UserSink:   "USERSINK",

	// Operators
	Assign:   ":=",
	Plus:     "+",
	Dash:     "-",
	Bang:     "!",
	Asterisk: "*",
	Slash:    "/",
	Modulo:   "%",
	Dot:      ".",
	Arrow:    "->",

	// PlusAssign:  "+=",
	// MinusAssign: "-=",
	// TimesAssign: "*=",
	// SlashAssign: "/=",
	// ModAssign:   "%=",

	LessThan:      "<",
	GreaterThan:   ">",
	LessThanEq:    "<=",
	GreaterThanEq: ">=",
	Equal:         "=",
	NotEqual:      "!=",

	// BitwiseAnd:    "&",
	// BitwiseOr:     "|",
	// BitwiseAndNot: "&^",

	// ShiftLeft:  "<<",
	// ShiftRight: ">>",

	// // Delimiters
	// Comma:     ",",
	Semicolon: ";",
	// Colon:     ":",
	// Carrot:    "^",

	// Groups and blocks
	LParen:  "(",
	RParen:  ")",
	LBrace:  "{",
	RBrace:  "}",
	LSquare: "[",
	RSquare: "]",

	// Keywords
	// Function: "func",
	// Let:      "let",
	// Const:    "const",
	// True:     "true",
	// False:    "false",
	// If:       "if",
	// Elif:     "elif",
	// Else:     "else",
	// Return:   "return",
	// Nil:      "nil",
	// LAnd:     "and",
	// LOr:      "or",
	// For:      "for",
	// While:    "while",
	// Continue: "continue",
	// Break:    "break",
	// Try:      "try",
	// Catch:    "catch",
	// Throw:    "throw",
	// Class:    "class",
	// New:      "new",
	// Pass:     "pass",
	// Import:   "import",
	// As:       "as",
	// Delete:   "delete",
	// Use:      "use",
}

var keywords map[string]TokenType

func init() {
	keywords = make(map[string]TokenType)
	for i := keywordBeg + 1; i < keywordEnd; i++ {
		keywords[tokens[i]] = i
	}
}

func LookupIdent(ident string) TokenType {
	if tok, ok := keywords[ident]; ok {
		return tok
	}
	return Identifier
}

func (t TokenType) String() string {
	if 0 <= t && t < TokenType(len(tokens)) {
		return tokens[t]
	}
	return "token(" + strconv.Itoa(int(t)) + ")"
}
